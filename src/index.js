import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
//import { BrowserRouter } from 'react-router-dom';
import App from './components/app';
import data from './data';

/*
const root = document.getElementById('powerman0-app-root');

root.setAttribute('data-ua', navigator.userAgent);
ReactDOM.render(
	<BrowserRouter>
		<App data={data} />
	</BrowserRouter>
, root);
*/

const root1 = document.getElementById('powerman1-app-root');
const root2 = document.getElementById('powerman2-app-root');
const root3 = document.getElementById('powerman3-app-root');
const root4 = document.getElementById('powerman4-app-root');
const root5 = document.getElementById('powerman5-app-root');

ReactDOM.render(<App idtype={1} data={data} />, root1);
ReactDOM.render(<App idtype={2} data={data} />, root2);
ReactDOM.render(<App idtype={3} data={data} />, root3);
ReactDOM.render(<App idtype={4} data={data} />, root4);
ReactDOM.render(<App idtype={5} data={data} />, root5);