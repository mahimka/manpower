import React from "react"
import * as d3 from 'd3';
import bind from '../../utils/bind';

export default class BarChart3 extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      country: props.country,
      wd: document.getElementById("powerman5-app-root").offsetWidth
    };

    bind(this, 'handleResize', 'createChart');

    this.colors = ["#466ea5", "#6390c6", "#6e8f82", "#e77c22", "#ab404b", "#494a4f", "#74757e", "#cacbce"];
    this.texts = ["Lack of applicants",
          "Lack of experience",
          "Applicants lack required hard skills",
          "Applicants lack required soft skills",
          "Applicants expect higher pay<br>than offered",
          "Applicants expecting better<br>benefits than offered",
          "An issue specific to my organisation",
          "Other/ don’t know"];
  }

  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
    this.createChart()
  }

  handleResize() {
    if(document.getElementById("powerman5-app-root").offsetWidth != this.state.wd){
      this.setState({
        wd: document.getElementById("powerman5-app-root").offsetWidth
      })

      d3.select(".pm-graph5-2-div").selectAll("*").remove();
      this.createChart();
    }
  }

  createChart(){
      this.g = d3.select(".pm-graph5-2-div");
      this.redrawGraph()   
  }

  componentDidUpdate() {
      this.redrawGraph()
  }

  createData(dt) {      
      let data = []
      dt.forEach(function(d, i) {
          if(i<=7){

            data.push({
              name: i,
              val: d
            });

          }

        });
      return data
  }



  redrawGraph(){
      let data = this.createData(this.props.data[this.props.country]);
      this.drawGraph(data);
  }

  drawGraph(dt){

    let that = this
    const wd = (this.state.wd <= 800) ? this.state.wd : ((this.state.wd < 1180) ? this.state.wd-320 : 860);
    const hg = (this.state.wd <= 800) ? 300 : 550
    const mrg = 10
    let sizeWd = ( wd - 3*mrg ) / (4*2)

    let bars = this.g.selectAll(".pm-whitebar2").data(dt)
    bars.enter()
        .append("div")
        .attr("class", "pm-whitebar2")
        .style("width", (d)=>{return d.val + '%'})
        .style("height", "50px")
        .style("background-color", (d, i) => { return that.colors[i] })
        .style("margin-bottom", (d, i) => { return ((i==3)||(i==4)) ? '45px' : '30px' })
        .html((d, i)=>{ 
          const span1 = '<span class="pm-whitespan2 pm-colorspan2'+i+'">' +  ((d.val==0)? '': d.val+'%') + '</span>'
          const span2 = '<div class="pm-whitespan3 pm-whitespan3'+i+'">' + this.texts[i] + '</div>'
          return span1 + span2 })

    bars
        .transition()
        .ease(d3.easeCubic)
        .duration(900)
        .style("width", (d)=>{return d.val + '%'})
    
    bars    
        .html((d, i)=>{ 
          const span1 = '<span class="pm-whitespan2 pm-colorspan2'+i+'">' +  ((d.val==0)? '': d.val+'%') + '</span>'
          const span2 = '<div class="pm-whitespan3 pm-whitespan3'+i+'">' + this.texts[i] + '</div>'
          return span1 + span2 })
    
  }

  render() {
      

    return (
      <div>
      <div className="pm-graph5-2-div"></div>
      </div>
    );
  }

}
