import React from 'react';
import html from 'react-inner-html';
import LineChart from '../lineChart';
import bind from '../../utils/bind';
import CustomSelect from '../customSelect'

export default class Graph1 extends React.Component {
  constructor(props) {
    super(props);
    
    let newWd = document.getElementById("powerman1-app-root").offsetWidth;

    this.dt = Object.keys(props.data)
    this.state = {
      country: (newWd <= 800) ? this.dt[1] : "",
      country2: (newWd <= 800) ? this.dt[2] : "",
      country3: "",
      wd: newWd
    };

    this.colors = ["#466ea5", "#6390c6", "#6e8f82", "#e77c22"];

    bind(this, 'update', 'update2', 'update3', 'handleResize');
  }


  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
  }

  handleResize() {
    let newWd = document.getElementById("powerman1-app-root").offsetWidth;
    if(newWd != this.state.wd){
      this.setState({
        wd: newWd
      })
    }
  }


  componentDidUpdate(){
  }


  update(country){
    this.setState({country: country})
  }
  update2(country){
    this.setState({country2: country})
  }
  update3(country){
    this.setState({country3: country})
  }

  render() {
    
    const albumOrientation = document.getElementById("powerman1-app-root").offsetWidth <= 800


    const header = (albumOrientation) ? "Compare countries" : this.props.header;
    const data = Object.keys(this.props.data)
    const fulldt = Object.keys(this.props.data);
    const glob = data[0];
    const dt = data.slice(1)
    const dt1 = dt.filter((t, i)=> { return ((t!=this.state.country2) && (t!=this.state.country3))})
    const dt2 = dt.filter((t, i)=> { return ((t!=this.state.country) && (t!=this.state.country3))})
    const dt3 = dt.filter((t, i)=> { return ((t!=this.state.country) && (t!=this.state.country2))})
    const cwd = 0;
    const selectWd = (albumOrientation) ? "" : 300

    return (
      <div id="pm-graph1" className="pm-graph">
        <div id="pm-controls1">
            <div id="pm-label1-1" className="pm-label">{header}</div>
            
            <div id="pm-selectGroup">
                
              <CustomSelect
                colored={this.colors[0]}
                coloredId={1}
                wd={selectWd}
                deleteable={(albumOrientation)?false:true}
                selection={this.state.country}
                data={dt1} 
                update={this.update}
              />

              <CustomSelect
                colored={(albumOrientation)?this.colors[3]:this.colors[1]}
                coloredId={(albumOrientation)?4:2}
                wd={selectWd}
                deleteable={(albumOrientation)?false:true}
                selection={this.state.country2}
                data={dt2} 
                update={this.update2}
              />

              <CustomSelect
                hide={(albumOrientation)?true:false}
                colored={this.colors[2]}
                coloredId={3}
                wd={selectWd}
                deleteable={true}
                selection={this.state.country3}
                data={dt3} 
                update={this.update3}
              />
            </div>

        </div>

        <LineChart 
          data={this.props.data}
          country1={this.state.country}
          country2={this.state.country2}
          country3={this.state.country3}
        />
      
      </div>
    );
  }
}

