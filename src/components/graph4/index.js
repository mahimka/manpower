import React from 'react';
import html from 'react-inner-html';
import bind from '../../utils/bind';
import BarChart from '../barChart';
import CustomSelect from '../customSelect'

export default class Graph4 extends React.Component {
  constructor(props) {
    super(props);
    
    this.dt = Object.keys(props.data)
    this.state = {
      country: this.dt[1]
    };

    bind(this, 'update');
  }

  update(country){
    this.setState({country: country})
  }

  render() {

    const label = "GLOBAL"
    const header = this.props.header;
    const data = Object.keys(this.props.data)
    const glob = data[0];
    const dt = data.splice(1);

    return (
      <div id="pm-graph4" className="pm-graph">
        <div id="pm-controls4">
          <div id="pm-label4-1" className="pm-label">{label}</div>
          <div id="pm-label4legend"></div>
          <div id="pm-label4-2" className="pm-label">{header}</div>
          <CustomSelect 
            className="customSelect" 
            data={dt} 
            update={this.update}
          />          
        </div>

        <BarChart 
          data={this.props.data}
          country={this.state.country}
        />
      
      </div>
    );
  }
}
