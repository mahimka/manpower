import React from "react"
import * as d3 from 'd3';
import bind from '../../utils/bind';

export default class BarChart extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      country: props.country,
      wd: document.getElementById("powerman4-app-root").offsetWidth
    };

    bind(this, 'handleResize', 'createChart');

    this.colors = ["#466ea5", "#6390c6", "#6e8f82", "#e77c22"]

  }

  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
    this.createChart()
  }

  handleResize() {
    if(document.getElementById("powerman4-app-root").offsetWidth != this.state.wd){
      this.setState({
        wd: document.getElementById("powerman4-app-root").offsetWidth
      })

      d3.select(".pm-graph4-div").selectAll("*").remove();
      this.createChart();
    }
  }

  createChart(){
      this.g = d3.select(".pm-graph4-div");

      this.createAxis()
      this.redrawGraph()   
  }

  componentDidUpdate() {
      this.redrawGraph()
  }

  createData(dt) {
    let data = []
    dt.forEach(function(d, i) {
        data.push({
          name: i,
          val: d
        });
      });
    return data
  }

  createAxis(){

    let that = this
    let gdata = this.createData(this.props.data["GLOBAL"]);
    const wd = (this.state.wd <= 800) ? this.state.wd : ((this.state.wd < 1180) ? this.state.wd-320 : 860);
    const hg = (this.state.wd <= 800) ? 270 : 550
    const mrg =  (this.state.wd <= 800)?10:((this.state.wd <= 1200)?30:70)
    const sizeWd = ( wd - 3*mrg ) / 4

    let bars = this.g.selectAll(".pm-blackbar").data(gdata)
    bars.enter()
        .append("div")
        .attr("class", (d, i)=>{ return "pm-blackbar pm-gradientbar" + i })
        .style("width", sizeWd * 2/3 + 'px')
        .style("height", (d)=>{ return (hg * (d.val / 100)) + 'px'})
        .style("left", function(d, i) { return ((sizeWd + mrg)*i + sizeWd/3) + 'px'; })
        .append("span")
        .attr("class", (d, i) => { return "pm-blackspan pm-colorspan" + i })
        .text((d)=>{return d.val + '%'})

  }

  redrawGraph(){
      let data = this.createData(this.props.data[this.props.country]);
      this.drawGraph(data);
  }

  drawGraph(dt){

    let that = this
    const wd = (this.state.wd <= 800) ? this.state.wd : ((this.state.wd < 1180) ? this.state.wd-320 : 860);
    const hg = (this.state.wd <= 800) ? 270 : 550
    const mrg = (this.state.wd <= 800)?10:((this.state.wd <= 1200)?30:70)
    let sizeWd = ( wd - 3*mrg ) / 4

    let bars = this.g.selectAll(".pm-whitebar").data(dt)
    bars.enter()
        .append("div")
        .attr("class", "pm-whitebar")
        .style("width", sizeWd * 2/3 + 'px')
        .style("height", (d)=>{ return (hg * (d.val / 100)) + 'px'})
        .style("left", function(d, i) { return ((sizeWd + mrg)*i) + 'px'; })
        .style("background-color", (d, i) => { return that.colors[i] })
        .html((d, i)=>{ 
            let global=[32,45,56,67];
            let delta = 5;
            let addcls = ""
            if((d.val >= global[i])&&(d.val < global[i] + delta))
              addcls = "antioverlapUp"
            if((d.val < global[i])&&(d.val > global[i] - delta))
              addcls = "antioverlapDown"
            let additionalClass = (that.state.wd >= 800)?"":addcls
            return '<span class="pm-whitespan pm-colorspan'+i+' '+additionalClass+'">' +  ((d.val==0)? '': d.val+'%') + '</span>' })
    bars
        .transition()
        .ease(d3.easeCubic)
        .duration(900)
        .style("height", (d)=>{ return (hg * (d.val / 100)) + 'px'})
    
    bars    
        .html((d, i)=>{ 
            let global=[32,45,56,67];
            let delta = 5;
            let addcls = ""
            if((d.val >= global[i])&&(d.val < global[i] + delta))
              addcls = "antioverlapUp"
            if((d.val < global[i])&&(d.val > global[i] - delta))
              addcls = "antioverlapDown"
            let additionalClass = (that.state.wd >= 800)?"":addcls
            return '<span class="pm-whitespan pm-colorspan'+i+' '+additionalClass+'">' +  ((d.val==0)? '': d.val+'%') + '</span>' })
    
  }

  render() {
      
    const svgWd = (this.state.wd <= 800) ? this.state.wd : ((this.state.wd < 1180) ? this.state.wd-320 : 860);
    const svgHg = (this.state.wd <= 800) ? 270 : 550
    const divStyle = {
      width: svgWd + 'px',
      height: svgHg + 'px'
    }
    const legStyle = {
      width: svgWd + 'px'
    }

    const mrg = (this.state.wd <= 800)?10:((this.state.wd <= 1200)?30:70)
    const sizeWd = ( svgWd - 3*mrg ) / 4
    const legStyleTxt = {
      width: sizeWd + 'px'
    }

    const vals1 = ["MICRO", "SMALL", "MEDIUM", "LARGE"]
    const vals2 = ["<10", "10-49", "50-250", "250+"]

    return (
      <div>
      <div className="pm-graph4-div" style={divStyle}></div>
      <div className="pm-legend" style={legStyle}>
        <div className="pm-legend-line0">
          <div className="pm-legend-text" style={legStyleTxt}><span>{vals1[0]}</span><br/><span className="pm-legendboldspan">{vals2[0]}</span></div>
          <div className="pm-legend-text" style={legStyleTxt}><span>{vals1[1]}</span><br/><span className="pm-legendboldspan">{vals2[1]}</span></div>
          <div className="pm-legend-text" style={legStyleTxt}><span>{vals1[2]}</span><br/><span className="pm-legendboldspan">{vals2[2]}</span></div>
          <div className="pm-legend-text" style={legStyleTxt}><span>{vals1[3]}</span><br/><span className="pm-legendboldspan">{vals2[3]}</span></div>
          
        </div>
      </div>
      </div>
    );
  }

}
