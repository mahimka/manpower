import React from 'react';
import html from 'react-inner-html';
import bind from '../../utils/bind';
import CustomSelect from '../customSelect'
import Cards from '../cards';

export default class Graph2 extends React.Component {
  constructor(props) {
    super(props);
    
    this.dt = Object.keys(props.data)
    this.state = { 
      country: this.dt[0]
    };

    bind(this, 'update');
  }

  componentDidMount() {     

  }

  update(country){
    this.setState({country: country})
  }

  render() {
    const header = this.props.header;
    const data = Object.keys(this.props.data)
    const fulldt = Object.keys(this.props.data);
    const glob = data[0];
    const dt = data;

    return (
      <div id="pm-graph2" className="pm-graph">
        
        <div id="pm-controls2">
            <div id="pm-label2" className="pm-label">{header}</div>
            <CustomSelect 
              className="customSelect" 
              data={dt} 
              update={this.update}
            />
        </div>

        <Cards 
          data={this.props.data}
          country={this.state.country}
        />
      </div>
    );
  }
}