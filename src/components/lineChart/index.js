import React from "react"
import * as d3 from 'd3';
import bind from '../../utils/bind';

export default class LineChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      country1: props.country1,
      country2: props.country2,
      country3: props.country3,
      wd: document.getElementById("powerman1-app-root").offsetWidth
    };

    bind(this, 'handleResize', 'createChart');

    this.tooltip = d3.select('body').append("div") 
      .attr("class", "pm-tooltip1")       
      .style("opacity", 0);

    this.colors = ["#466ea5", "#6390c6", "#6e8f82", "#e77c22"];

    this.boundWidth = 800;
    this.boundWidth2 = 1050;

  }

  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
    this.createChart()
  }

  handleResize() {
    if(document.getElementById("powerman1-app-root").offsetWidth != this.state.wd){
      this.setState({
        wd: document.getElementById("powerman1-app-root").offsetWidth
      })

      d3.select(".pm-graph1-svg").selectAll("*").remove();
      this.createChart();
    }
  }

  createChart(){
      let svg = d3.select(".pm-graph1-svg");
      let margin = {top: (this.state.wd <= 800) ? 20:20, right: (this.state.wd <= 800) ? 10 : 20, bottom: 40, left: (this.state.wd <= 800) ? 10:40};
      this.width = +svg.attr("width") - margin.left - margin.right,
      this.height = +svg.attr("height") - margin.top - margin.bottom
      this.g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      this.createAxis()
      this.redrawGraph()
  }

  componentDidUpdate() {
      this.redrawGraph()
  }

  createData(dt) {
      let year = 2006
      let data = []
      if(dt){
      dt.forEach(function(d) {
          if(d!=0){
             data.push({
              dt: new Date(year, 0, 1),
              val: d
            });
          }
          year++;
        });
      }
      return data
  }

  createAxis(){

    let that = this
    let gdata = this.createData(this.props.data["GLOBAL"]);

    this.x = d3.scaleTime()
        .domain([new Date(2005, ((this.state.wd <= 800) ? 10 : 10), 1), new Date(2018, 2, 1)])
        .range([0, this.width]);

    this.y = d3.scaleLinear()
        .domain([0, 100])
        .range([this.height, 0]);

    let xAxis = d3.axisBottom(this.x)
        .ticks(d3.timeYear)
        .tickFormat(function(d) {
          let a = d.getFullYear()
          if(a == 2018) a = 2019
          if(a == 2017) a = 2018
          return a;
        });


    let yAxis = d3.axisRight(this.y)
        .tickSize(this.width)
        .tickFormat(function(d) { return d; });

    let line = d3.line()
      .x(function(d) { return that.x(d.dt); })
      .y(function(d) { return that.y(d.val); }); 

    this.g.append("g")
        .attr("transform", "translate(0," + this.height + ")")
        .call(customXAxis);

    this.g.append("g")
        .attr("transform", "translate(0,0)")
        .call(customYAxis);

    function customXAxis(g) {
      g.call(xAxis);
      g.select(".domain").remove();
      g.selectAll(".tick line").attr("stroke", "#acadb3").attr("y2", 0);
      g.selectAll(".tick:first-of-type line")
        .attr("y2", (that.state.wd <= that.boundWidth) ? -that.height : -that.height)
        .attr("y1", (that.state.wd <= that.boundWidth) ? 5 : 10);
      g.selectAll(".tick text")
        .attr("dy", (that.state.wd <= that.boundWidth) ? 6 : 20)
        .attr("fill", "#acadb3")
        .attr("font-size", (that.state.wd <= that.boundWidth) ? "10px" : "18px")
        .attr("font-family", (that.state.wd <= that.boundWidth) ? "Helvetica Neue Web" : "Helvetica Neue Web Bd");

      if(that.state.wd <= that.boundWidth){
        g.selectAll(".tick text").attr("opacity", 0)
        g.selectAll(".tick:first-of-type text").attr("opacity", 1)
        g.selectAll(".tick:last-of-type text").attr("opacity", 1)
      }

    }

    function customYAxis(g) {
      g.call(yAxis);
      g.select(".domain").remove();
      g.selectAll(".tick line").attr("stroke", "#acadb3");
      g.selectAll(".tick:not(:first-of-type) line").attr("stroke-dasharray", "2,2").attr("stroke-opacity", "0");
      g.selectAll(".tick text")
        .attr("x", (that.state.wd <= that.boundWidth) ? -5 : 4)
        .attr("dy", (that.state.wd <= that.boundWidth) ? -6 : 6)
        .attr("fill", "#acadb3")
        .attr("text-anchor",  (that.state.wd <= that.boundWidth) ? "start" : "end")        
        .attr("font-size", (that.state.wd <= that.boundWidth) ? "10px" : "18px")
        .attr("font-family", (that.state.wd <= that.boundWidth) ? "Helvetica Neue Web" : "Helvetica Neue Web Bd");
      g.selectAll(".tick:first-of-type text").attr("opacity", 0)
      
      if(that.state.wd <= that.boundWidth){
        g.selectAll(".tick text").attr("opacity", 0)
        g.selectAll(".tick:first-of-type text").attr("opacity", 1)
        g.selectAll(".tick:last-of-type text").attr("opacity", 1)
      }
    }  

        // d3.selectAll(".tick>text")
        // .nodes()
        // .map(function(t){
        //   let a = t.innerHTML
        //   if(a == '2018') a = '2019'
        //   if(a == '2017') a = '2018'
        //   console.log(a)
        //   return a;
        // })


    // black graph
    this.black = this.g.append("g").attr('id', 'blackg')
    this.black.append("path")
        .attr("class", "blackPath")
        .attr("fill", "none")
        .attr("stroke", '#494a4f')
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", 2)
        .attr("opacity", (that.state.wd <= that.boundWidth)?1:1)
        .attr("d", line(gdata));

    let dots = this.black.selectAll("circle").data(gdata)
    dots.enter()
        .append("circle")
        .attr("r", ((this.state.wd < that.boundWidth) ? 4 : 6))
        .attr("fill", '#494a4f')
        .attr("stroke", '#494a4f')
        .attr("stroke-width", '20px')
        .attr("stroke-opacity", 0)
        .style("display", (d)=>{ return (that.state.wd < that.boundWidth) ? "none" : "block" })
        .attr("cx", function(d) { return that.x(d.dt); })
        .attr("cy", function(d) { return that.y(d.val); })
        .on('mouseover', (d)=>{
          
          let rootmargin = parseFloat(d3.select('#powerman1-app-root').style('margin-top'))
          let containerMargin = 0//document.getElementById("powerman1-app-root").offsetTop;
          let containerMarginLeft = document.getElementById("powerman1-app-root").offsetLeft;
          this.tooltip
              .style("background", '#494a4f')  
              .style("color", 'white') 
              .transition()    
              .duration(300)
              .style("opacity", .9);    
          this.tooltip.html(d.val + '%')  
              .style("left", (d3.event.pageX +5) + "px")   
              .style("top", (d3.event.pageY - 28 - 5) + "px");          
        })
        .on("mousemove", (d)=>{    
          let rootmargin = parseFloat(d3.select('#powerman1-app-root').style('margin-top'))
          let containerMargin = 0//document.getElementById("powerman1-app-root").offsetTop;
          let containerMarginLeft = document.getElementById("powerman1-app-root").offsetLeft;
          this.tooltip
              .style("left", (d3.event.pageX+5) + "px")   
              .style("top", (d3.event.pageY - 28 - 5) + "px");
        })  
        .on("mouseout", (d)=>{    
            this.tooltip.transition()    
              .duration(0).style("opacity", 0); 
        }); 

    


    // white graph 1
    this.white1 = this.g.append("g").attr('id', 'whiteg')
    this.white1.append("path")
        .attr("class", "whitePath1")
        .attr("fill", "none")
        .attr("stroke", that.colors[0])
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", 2)
        .attr( "d", line( that.createData( that.props.data[that.props.country1] ).reverse() ) );

    // white graph 2
    this.white2 = this.g.append("g").attr('id', 'whiteg2')
    this.white2.append("path")
        .attr("class", "whitePath2")
        .attr("fill", "none")
        .attr("stroke", (this.state.wd <= that.boundWidth)?that.colors[3]:that.colors[1])
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", 2)
        .attr("d", line(that.createData(that.props.data[that.props.country2]).reverse()));

    // white graph 3
    this.white3 = this.g.append("g").attr('id', 'whiteg3')
    this.white3.append("path")
        .attr("class", "whitePath3")
        .attr("fill", "none")
        .attr("stroke", that.colors[2])
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", 2)
        .attr("d", line(that.createData(that.props.data[that.props.country3]).reverse()));
    
  }

  redrawGraph(){
      let data1 = this.createData(this.props.data[this.props.country1]);
      let data2 = this.createData(this.props.data[this.props.country2]);
      let data3 = this.createData(this.props.data[this.props.country3]);
      this.drawGraph(data1, 1);
      this.drawGraph(data2, 2);
      this.drawGraph(data3, 3);
  }

  drawGraph(dt, n){

    let that = this
    let line = d3.line()
      .x(function(d) { return that.x(d.dt); })
      .y(function(d) { return that.y(d.val); });  

    //line
    this["white" + n].select(".whitePath" + n)   
            .transition()
            .ease(d3.easeCubic)
            .duration(900)
            .attr("d", line(dt.reverse()));

    // points
    let dots = this["white" + n].selectAll("circle").data(dt, function(d) { return d.dt; })

    dots.enter()
        .append("circle")
        .attr("r", ((this.state.wd <= that.boundWidth) ? 4 : 6))
        .attr("fill", that.colors[n-1])
        .attr("stroke", that.colors[n-1])
        .attr("stroke-width", '20px')
        .attr("stroke-opacity", 0)
        .style("opacity", (d)=>{ return (that.state.wd <= that.boundWidth) ? 0 : 1 })
        .attr("cx", function(d) { return that.x(d.dt); })
        .attr("cy", function(d) { return that.y(d.val); })
        .on('mouseover', (d)=>{
          
          let rootmargin = parseFloat(d3.select('#powerman1-app-root').style('margin-top'))
          let containerMargin = 0//document.getElementById("powerman1-app-root").offsetTop;
          let containerMarginLeft = document.getElementById("powerman1-app-root").offsetLeft;
          

          let clr = that.colors[n-1];
          if(that.state.wd <= that.boundWidth){
            if(n-1 > 0)
              clr = that.colors[3]
          }

          this.tooltip
            .style("background", clr)  
            .transition()    
            .duration(300)
            .style("opacity", .9);    
          this.tooltip.html(d.val + '%')  
            .style("left", (d3.event.pageX +5) + "px")   
            .style("top", (d3.event.pageY - 28 - 5) + "px");          
        })
        .on("mousemove", (d)=>{  
          let rootmargin = parseFloat(d3.select('#powerman1-app-root').style('margin-top'))  
          let containerMargin = 0//document.getElementById("powerman1-app-root").offsetTop;
          let containerMarginLeft = document.getElementById("powerman1-app-root").offsetLeft;
          this.tooltip.style("left", (d3.event.pageX + 5) + "px")   
             .style("top", (d3.event.pageY - 28 - 5 ) + "px");
        })  
        .on("mouseout", (d)=>{    
            this.tooltip.transition()    
              .duration(0).style("opacity", 0); 
        });        

    dots.transition()
        .ease(d3.easeCubic)
        .duration(900)
        .attr("cx", function(d) { return that.x(d.dt); })
        .attr("cy", function(d) { return that.y(d.val); }); 

    dots.exit().remove() 
  }

  render() { 
    const svgWd = (this.state.wd <= this.boundWidth2) ? this.state.wd : this.boundWidth2;
    const svgHg = (this.state.wd <= this.boundWidth) ? 180 : 530;

    return (
      <div className="pm-graph1-container">
        <svg className="pm-graph1-svg" width={svgWd} height={svgHg}></svg>
        <div className="pm-label1-2 pm-label">{"GLOBAL"}</div>
      </div>
    );
  }

}  