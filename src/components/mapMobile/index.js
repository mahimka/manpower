import React from "react"
import * as d3 from 'd3';
import bind from '../../utils/bind';

export default class mapMobile extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      country: props.country,
      wd: document.getElementById("powerman3-app-root").offsetWidth
    };

    this.colors = ["#466ea5", "#e77c22", "#494a4f", "#6e8f82", "#ab404b"]
    this.values = ["Least difficulty filling positions","Below average difficulty filling positions", "Average difficulty filling positions", "Above average difficulty filling positions","Most difficulty filling positions"];
  }

  getIntervalId(nmb){
    if(nmb>51){
      return 4
    }
    else if(nmb>45){
      return 3
    }
    else if(nmb==45){
      return 2
    }
    else if(nmb>33){
      return 1
    }
    else {
      return 0
    }
  }

  render() {
      
    const nmb = Math.round(this.props.data[this.props.country]);
    const id = this.getIntervalId(nmb)

    return (
      <div style={{color: this.colors[id]}}>
        <div className="pm-map-mobile-number">
          <span>{nmb}</span>
          <span className="pm-percent">{"%"}</span>
        </div>
        <div className="pm-blueline" style={{backgroundColor: this.colors[id]}}></div>
        <div className="pm-interval">{this.values[id] + "*"}</div>
      </div>
    );
  }

}
