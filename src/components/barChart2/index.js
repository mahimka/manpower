import React from "react"
import * as d3 from 'd3';
import bind from '../../utils/bind';

export default class BarChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      country: props.country,
      wd: window.innerWidth
    };

    bind(this, 'handleResize', 'createChart', 'hoverEffect');
    this.handleOver = this.handleOver.bind(this)
    this.handleOut = this.handleOut.bind(this)


    this.texts = ["Lack of applicants",
          "Lack of experience",
          "Applicants lack required hard skills",
          "Applicants lack required soft skills",
          "Applicants expect higher pay than offered",
          "Applicants expecting better benefits than offered",
          "An issue specific to my organization",
          "Other/ don’t know"]

    this.colors = ["#466ea5", "#6390c6", "#6e8f82", "#e77c22", "#ab404b", "#494a4f", "#74757e", "#cacbce"];

    this.minpercent = 2;
  }

  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
    this.createChart()
  }

  handleResize() {
    if(window.innerWidth != this.state.wd){
      this.setState({ wd: window.innerWidth })
      d3.select(".pm-graph5-div").selectAll("*").remove();
      this.createChart();
    }
  }

  createChart(){
      this.g = d3.select(".pm-graph5-div");
      
      this.createAxis()
      this.redrawGraph()   
  }

  componentDidUpdate() {
      this.redrawGraph()
  }

  createData(dt) {      
      let data = []
      let total = 0
      dt.forEach(function(d, i) {          
          if(i<dt.length-2)
            total += d
          data.push({
            name: i,
            val: d
          });
        });
      //data[data.length-2].val += data[data.length-1].val
      data[data.length-2].val = 100 - total;
      data.pop();
      return data
  }

  createAxis(){

    let that = this
    let gdata = this.createData(this.props.data["GLOBAL"]);

    let globalbar = this.g.append('div').attr("class", "pm-globalbar")
    let countrybar = this.g.append('div').attr("class", "pm-countrybar")

    let bars = globalbar.selectAll(".pm-globalbars").data(gdata)
    bars.enter()
        .append("div")
        .attr("class", "pm-globalbars")
        .attr("id", (d, i)=> { return "pm-globalbars" + i})
        .style("width", (d)=> { return ((that.state.wd > 800) ? d.val : 100) + '%'})
        .style("height", (d)=> { return ((that.state.wd > 800) ? 100 : d.val) + '%'})
        .style("background-color", (d, i)=>{ return that.colors[i]})
        .html((d)=>{ 
          let clss = (d.val < that.minpercent) ? ' pm-g5spanDisable' : ''
          return '<span class="pm-g5span'+clss+'">' +  d.val + '%</span>' })
        .on('mouseover', (d)=>{ that.hoverEffect(d.name, true) })
        .on('mouseout', (d)=>{ that.hoverEffect(d.name, false) })

  }

  hoverEffect(id, bool){
      let that = this;
      d3.select('#pm-globalbars' + id).classed('pm-globalbarsSelected', bool)
      d3.select('#pm-countrybars' + id).classed('pm-countrybarsSelected', bool)
      //d3.select('#legend5Btn' + id).select(".legend5BtnText").style('background-color', (bool)?that.colors[id]:'white')
      d3.select('#pm-legend5Btn' + id).select(".pm-legend5BtnText").style('color', (bool)?that.colors[id]:'#494a4f')
      d3.select('#pm-legend5Btn' + id).select(".pm-legend5BtnText").classed('pm-legend5BtnSelected', bool)
      d3.select('#pm-legend5Btn' + id).select(".pm-legend5BtnColorContent").classed('pm-legend5BtnColorSelected', bool)
  }

  redrawGraph(){
      let data = this.createData(this.props.data[this.props.country]);
      this.drawGraph(data);
  }

  drawGraph(dt){

    let that = this
    let bars = this.g.select('.pm-countrybar').selectAll(".pm-countrybars").data(dt)
    bars.enter()
        .append("div")
        .attr("class", "pm-countrybars")
        .attr("id", (d, i)=> { return "pm-countrybars" + i})
        .style("width", (d)=> { return ((that.state.wd > 800) ? d.val : 100) + '%'})
        .style("height", (d)=> { return ((that.state.wd > 800) ? 100 : d.val) + '%'})
        .style("background-color", (d, i)=>{ return that.colors[i]})
        .html((d)=>{ 
          let clss = (d.val < that.minpercent) ? ' pm-g5spanDisable' : ''
          return '<span class="pm-g5span'+clss+'">' +  d.val + '%</span>' })
        .on('mouseover', (d)=>{ that.hoverEffect(d.name, true) })
        .on('mouseout', (d)=>{ that.hoverEffect(d.name, false) })
    bars
        .transition()
        .ease(d3.easeCubic)
        .duration(900)
        .style("width", (d)=> { return ((that.state.wd > 800) ? d.val : 100) + '%'})
        .style("height", (d)=> { return ((that.state.wd > 800) ? 100 : d.val) + '%'})
    bars    
        .html((d)=>{           
          let clss = (d.val < that.minpercent) ? ' pm-g5spanDisable' : ''
          return '<span class="pm-g5span'+clss+'">' +  d.val + '%</span>' })
    

  }

  handleOver(e){
    let txt = d3.select(e.target).select('.pm-legend5BtnText').text()
    this.hoverEffect(this.texts.indexOf(txt), true)
  }

  handleOut(e){
    let txt = d3.select(e.target).select('.pm-legend5BtnText').text()
    this.hoverEffect(this.texts.indexOf(txt), false)
  }




  render() {
      
    const divStyle = { width: '100%' }

    return (
      <div style={divStyle}>
        <div className="pm-graph5-div"></div>
        <div className="pm-legend5">
          <div className="pm-legend5Header">{'Colour key'}</div>
          <div className="pm-legend5Btns">
              {
                this.colors.map((t, i) => (                                        
                  <div key={i} className="pm-legend5Btn" id={"pm-legend5Btn" + i} onMouseOver={this.handleOver} onMouseOut={this.handleOut}>
                    <div className="pm-legend5BtnColor">
                      <div className="pm-legend5BtnColorContent" style={{backgroundColor: this.colors[i]}}></div>
                    </div>
                    <div className="pm-legend5BtnText">{this.texts[i]}</div>
                  </div>
                ))
              }
          </div>  
        </div>
      </div>
    );
  }

}
