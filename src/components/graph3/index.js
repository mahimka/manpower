import React from 'react';
import html from 'react-inner-html';
import Map from '../map';
import MapMobile from '../mapMobile';
import bind from '../../utils/bind';
import CustomSelect from '../customSelect'

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

export default class Graph3 extends React.Component {
  constructor(props) {
    super(props);
    
    this.dt = Object.keys(props.data)
    this.state = {
      interval: 0,
      wd: document.getElementById("powerman3-app-root").offsetWidth,
      country: this.dt[0],
    };

    bind(this, 'update', 'handleResize', 'update2');

    this.values = ["Least difficulty filling positions","Below average difficulty","Above average difficulty","Most difficulty filling positions"];
  }


  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
  }

  handleResize() {
    let newWd = document.getElementById("powerman1-app-root").offsetWidth;
    if(newWd != this.state.wd){
      this.setState({
        wd: newWd
      })
    }
  }

  update(val){
    this.setState({interval: val})
  }

  update2(val){
    this.setState({country: val})
  }

  render() {
    let that = this
    const header = this.props.header;
    const data = Object.keys(this.props.data)
    const glob = data[0];
    const dt = data.splice(1);

    const mobileView = (this.state.wd <= 800)
    const preffix = 'https://cdn2.hubspot.net/hubfs/4649810/infographic-assets/'//'assets/img/'
    return (
      <div id="pm-graph3" className="pm-graph">
        
        { mobileView ? (

          <div id="pm-controls3-2">
            <div id="pm-label3-2" className="pm-label">{"Compare with global average"}</div>
            <CustomSelect 
              className="customSelect" 
              data={Object.keys(this.props.data)} 
              update={this.update2}
            />
            <MapMobile
              data={this.props.data}
              interval={this.state.interval} 
              country={this.state.country}           
            />
            <div id="pm-label3-3" className="pm-label">{"*Global average 45%"}</div>
          </div>

          ) : (

          <div>
          <div id="pm-controls3">
            <img
                className="pm-mapglobalicon"
                src={preffix+'mapglobal.svg'}
              />
            <div id="pm-label3-1" className="pm-label">{header}</div>
            <div className="mp-menuc">
                {that.values.map((t, i) => (                      
                  
                  <Button 
                    key={i}
                    id={"mp-button3-"+i}
                    variant="outlined" 
                    className="mp-button3"
                    className={(i!=that.state.interval) ? "mp-button" : "mp-button mp-buttonSelected-"+i}
                    onClick={() => that.update(i)}
                  >
                    {t}
                  </Button>
                ))}
            </div>
          </div>
          <Map
            data={this.props.data}
            interval={this.state.interval}
          />
          </div>
          )
        }

      </div>
    );
  }
}