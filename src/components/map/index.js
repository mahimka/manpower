import React from "react"
import * as d3 from 'd3';
import {geoMiller} from "d3-geo-projection";
import '../../utils/queue.v1.min.js';
import '../../utils/topojson.v1.min.js';
import bind from '../../utils/bind';
import geojson from '../../data/world_countries.json';

export default class Map extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      interval: props.interval,
      wd: window.innerWidth, 
      zoom: 1,
    };

    bind(this, 'handleResize', 'createChart', 'getWd');

    this.tooltip = d3.select('body').append("div") 
      .attr("class", "pm-tooltip2")
      .style("opacity", 0);

    this.colors = ["#466ea5", "#e77c22", "#6e8f82", "#ab404b"];

  }

  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
    this.createChart()
  }

  handleResize() {
    if(window.innerWidth != this.state.wd){
      this.setState({
        wd: window.innerWidth
      })

      d3.select(".mp-graph3-svg").selectAll("*").remove();
      this.createChart();
    }
  }

  createChart(){
    let that = this
    let width = this.getWd()
    let height = 500
    this.zoom = 0

    d3.select('.mp-mapcontainer')
      .on('click', ()=>{ 
        if(that.zoom!=0)
          mapzoom(0) 
      });

    d3.select('.mp-map-controls')
      .style("display", (that.zoom==0)?"block":"none")
      .style("opacity", 1)

    d3.select('#mp-mb1').on('click', ()=>{ d3.event.stopPropagation();mapzoom(1) });
    d3.select('#mp-mb2').on('click', ()=>{ d3.event.stopPropagation();mapzoom(2) });
    d3.select('#mp-mb3').on('click', ()=>{ d3.event.stopPropagation();mapzoom(3) });      


    this.svg = d3.select(".mp-graph3-svg").append('g').attr('class', 'map')      

    let projection = geoMiller()
    projection.fitExtent([[0, 0], [width, 500]], geojson); 
    this.path = d3.geoPath().projection(projection);
    var zoom = d3.zoom()
      .scaleExtent([1, 8])
      .on("zoom", zoomed);

    function zoomed() {      
      that.svg.attr("transform", d3.event.transform);
    }

    function mapzoom(z){
      let scale = 1;
      let translate = [0, 0]
      
      that.zoom = z

      d3.select('.mp-map-controls')
        .style("display", (z==0)?"block":"none")
        .style("opacity", 0)
      .transition()
        .ease(d3.easeCubic)
        .duration(900)
        .style("opacity", (z==0)?1:0)

        let zoomoutx = 0;
        let zoomouty = 0;
        if(z==1){
          zoomoutx = 80;
          zoomouty = 55;
        }
        if(z==2){
          zoomoutx = 29;
          zoomouty = 35;
        }
        if(z==3){
          zoomoutx = 30;
          zoomouty = 55;
        }        

      d3.select('#mp-mbout')
        .style("display", (z==0)?"none":"block")
        .style("left", zoomoutx + '%')
        .style("top", zoomouty + '%')
        .style("opacity", 0)
      .transition()
        .ease(d3.easeCubic)
        .duration(900)
        .style("opacity", (z==0)?0:1)

      if(z==1){ scale = 4; translate = [width/2 - scale * (width/1.3), (height/2 - scale*height/1.8)]}
      if(z==2){ scale = 3; translate = [width/2 - scale * (width/1.9), (height/2 - scale*height/3.2)]}
      if(z==3){ scale = 2; translate = [width/2 - scale * (width/3.3), (height/2 - scale*height/1.6)]}

      that.svg
      .transition()
        .ease(d3.easeCubic)
        .duration(900)
      .call( zoom.transform, d3.zoomIdentity.translate(translate[0],translate[1]).scale(scale));
      that.setState({ zoom: (z==0)?1:2 })


      that.svg.selectAll("path").attr('stroke-width', 0.5)
    }

    this.redrawGraph()   
  }

  getMaxMin(i){
    if(i==3){
      return {max: 100, min: 52}
    }
    else if(i==2){
      return {max: 51, min: 45}
    }
    else if(i==1){
      return {max: 44, min: 34}
    }
    else if(i==0){
      return {max: 33, min: -1}
    }
  }

  componentDidUpdate() {
      this.redrawGraph()
  }

  redrawGraph(){
      this.drawGraph();
  }

  drawGraph(){

    let that = this
    const borderColor = "#58595b";
    const activeColor = "#6e8f82";
    const passiveColor = "#dedede";
    const passiveDarkColor = "#bbb";

    let countries = this.svg.selectAll("path").data(geojson.features)

    countries
        .enter()
        .append("path")
        .attr("d", that.path)  
        .attr('class', 'mp-country')
        .style("opacity",1)
        .style("fill", function(d) {
            let nm= d.properties.name.toUpperCase()
            if(nm=="ENGLAND") nm = "UK"
            let val = that.props.data[nm]
            


            return ( that.isActive(d.properties.name.toUpperCase())) ? that.colors[that.getColor(val)] : ((val) ? passiveDarkColor : passiveColor) 
        })
        .style('cursor', (d)=>{
            let nm= d.properties.name.toUpperCase()
            if(nm=="ENGLAND") nm = "UK"
            let val = that.props.data[nm]
            return (val) ? 'pointer' : 'auto'
        })
        
        .on('mouseover',function(d){
          
          let nm= d.properties.name.toUpperCase()
          if(nm=="ENGLAND") nm = "UK"
          let val = that.props.data[nm]

          if(val){
             d3.select(this)
                .transition()
                .ease(d3.easeCubic)
                .duration(300)
                .style("fill", function(d) {
                  let nm= d.properties.name.toUpperCase()
                  if(nm=="ENGLAND") nm = "UK"
                  let val = that.props.data[nm]
                  return that.colors[that.getColor(val)]
                })
                .style("opacity", 0.75)

            let val = that.props.data[nm];
            let col = that.colors[that.getColor(val)];
                val = Math.round(val) + '%'

            let rootmargin = parseFloat(d3.select('#powerman3-app-root').style('margin-top'))
            let containerMargin = 0//document.getElementById("powerman3-app-root").offsetTop;
            let containerMarginLeft = document.getElementById("powerman3-app-root").offsetLeft;
            that.tooltip
                .style("background", col)  
                .style("color", 'white') 
                .transition()    
                .duration(300)
                .style("opacity", .9);    
            that.tooltip.html(nm + ', ' + '<span>' + val + '</span>')  
                .style("left", (d3.event.pageX + 5) + "px")   
                .style("top", (d)=>{
                
                  return (d3.event.pageY - 28 - 5) + "px"});   
          }              
        })
        .on('mouseout', function(d){
           
            let nm= d.properties.name.toUpperCase()
            if(nm=="ENGLAND") nm = "UK"
            let val = that.props.data[nm]

            if(val){
             d3.select(this)
                .transition()
                .ease(d3.easeCubic)
                .duration(300)
                .style("fill", function(d) {
                  let nm= d.properties.name.toUpperCase()
                  if(nm=="ENGLAND") nm = "UK"
                  let val = that.props.data[nm]
                  return ( that.isActive(d.properties.name.toUpperCase())) ? that.colors[that.getColor(val)] : ((val) ? passiveDarkColor : passiveColor) })
                .style("opacity", 1)
                
                that.tooltip.transition()    
                  .duration(0).style("opacity", 0);
            } 
        })
        .on("mousemove", (d)=>{    
            let nm= d.properties.name.toUpperCase()
            if(nm=="ENGLAND") nm = "UK"
            let val = that.props.data[nm]
            let rootmargin = parseFloat(d3.select('#powerman3-app-root').style('margin-top'))
            let containerMarginLeft = document.getElementById("powerman3-app-root").offsetLeft;
            let containerMargin = 0//document.getElementById("powerman3-app-root").offsetTop;

            if(val){
              that.tooltip
                .style("left", (d3.event.pageX + 5) + "px")   
                .style("top", (d)=>{ return (d3.event.pageY - 28 - 5) + "px"}) }
        }) 
         
  

     countries
        .style('cursor', (d)=>{
            let nm= d.properties.name.toUpperCase()

            if(nm=="ENGLAND") nm = "UK"
            let val = that.props.data[nm]
            return (val) ? 'pointer' : 'auto'
        })
        .transition()
        .ease(d3.easeCubic)
        .duration(900)
        .style("fill", function(d) {
              let nm= d.properties.name.toUpperCase()
              if(nm=="ENGLAND") nm = "UK"
              let val = that.props.data[nm]
              return ( that.isActive(d.properties.name.toUpperCase())) ? that.colors[that.getColor(val)] : ((val) ? passiveDarkColor : passiveColor) 
        })
        
  }

  getColor(val){
    if(val >51) return 3
    if(val >=45) return 2
    if(val >33) return 1
    return 0
  }

  isActive(nm){
    if(nm=="ENGLAND") nm = "UK"
    let percent = this.props.data[nm]
    let interval = this.getMaxMin(this.props.interval)
    if((percent <= interval.max) && (percent >= interval.min))
      return true
    else
      return false
  }

  getWd(){
    
    let wd = document.getElementById("powerman3-app-root").offsetWidth - 200
    return wd;
  }


  render() {

    const svgWd = this.getWd()
    let btnsstyle = {
      width: svgWd,
      height: svgWd ,
      top: (500-svgWd)/2

    }
    const zoomtext = (this.state.zoom == 1) ? "Click on the zoom icon to zoom into the map" : "Click on the map to zoom out";

    const preffix = 'https://cdn2.hubspot.net/hubfs/4649810/infographic-assets/'//'assets/img/'

    return (
      <div className="mp-supermap">
        <div className="mp-maplabel">{zoomtext}</div>
        <div className="mp-mapcontainer">
          <svg className="mp-graph3-svg" width={svgWd} height="500"></svg>
          <div className="mp-map-controls" style={btnsstyle}>
            <img
                className="mp-map-button"  
                id="mp-mb1"
                src={preffix + 'zoom.svg'}
              />
           <img
                className="mp-map-button"  
                id="mp-mb2"
                src={preffix + 'zoom.svg'}
              />
           <img
                className="mp-map-button"  
                id="mp-mb3"
                src={preffix + 'zoom.svg'}
              />
          </div>
           <img
              className="mp-map-button"  
              id="mp-mbout"
              src={preffix + 'zoomout.svg'}
            />
        </div>
      </div>
    );
  }

}
