import React from "react"
import bind from '../../utils/bind';

export default class CustomSelect extends React.Component {

  constructor(props) {
    super(props);

    this.dt = Object.values(props.data)
    this.state = {
      selection: ((props.selection)||(props.selection=="")) ? ((props.selection=="") ? "CHOOSE A COUNTRY" : props.selection) : this.dt[0],
      open: false
    };

    bind(this, 'update', 'open', 'close', 'handleClickOutside', 'setWrapperRef');
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

   handleClickOutside() {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      if(this.state.open){
        this.setState({open: false})
      }
    }
  }

  open(){
    this.setState({open: !this.state.open})
  }

  close(e){
    if(this.props.deleteable && (this.state.selection != "CHOOSE A COUNTRY")){
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      this.setState({selection: "CHOOSE A COUNTRY", open: false})
      this.props.update("");
    }
  }

  update(country){
    this.setState({selection: country, open: false})
    this.props.update(country);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  
  render() {
    
    this.dt = Object.values(this.props.data)

    const csStyle = {
      width: (this.props.wd) ? (this.props.wd+'px') : ('100%'),
      display: (this.props.hide) ? 'none' : 'block',
    }

    const selectStyle = {
      borderColor: (this.props.colored) ? this.props.colored : "#494a4f",
      backgroundColor: (this.props.deleteable && (this.state.selection != "CHOOSE A COUNTRY")) ? ((this.props.colored) ? this.props.colored : "#fff") : "#fff",
      color: (this.props.colored) ? ((this.props.deleteable && (this.state.selection != "CHOOSE A COUNTRY"))? "#fff" : this.props.colored) : "#494a4f"
    }

    const iconStyle = {
      backgroundColor: (this.props.colored) ? this.props.colored : "#494a4f"
    }

    const dcStyle = {
      borderColor: (this.props.colored) ? this.props.colored : "#494a4f",
      color: (this.props.colored) ? this.props.colored : "#494a4f"
    }

    const elSelected = (!this.props.coloredId) ? "dropdownElement dropdownElementSelected" : "dropdownElement dropdownElementSelected"
    const elUnselected = (!this.props.coloredId) ? "dropdownElement" : "dropdownElement dropdownElementColored" + this.props.coloredId
    const icon = (this.props.deleteable && (this.state.selection != "CHOOSE A COUNTRY")) ? 'cross' : 'arrow'


    const preffix = 'https://cdn2.hubspot.net/hubfs/4649810/infographic-assets/'//'assets/img/'

    return (
      <div className={(this.props.coloredId) ? ((this.state.open)?"customSelect2 csopened":"customSelect2") : ((this.state.open)?"customSelect csopened":"customSelect")} ref={this.setWrapperRef} style={csStyle}>
        <div className={(this.state.open) ? "selectContainer selectContainerOpen" : "selectContainer" } onClick={this.open} style={selectStyle}>
          <div className="selectText">{this.state.selection}</div>
          <div className="selectIcon" onClick={this.close} style={iconStyle}>
            <img src={preffix+icon+'.svg'} />
          </div>
        </div>
        <div style={dcStyle} className={(this.state.open) ? "dropdownContainer dropdownContainerOpen" : "dropdownContainer"}>
          {this.dt.map((t, i) => (       
              <div                 
                className={(t==this.state.selection) ? elSelected : elUnselected} 
                key={i} value={t} onClick={() => this.update(t)}>{t}</div>
            ))}
        </div>
      </div>
    );
  }

}
