import React from 'react';
import html from 'react-inner-html';
import bind from '../../utils/bind';
import BarChart2 from '../barChart2';
import BarChart3 from '../barChart3';
import CustomSelect from '../customSelect'

export default class Graph5 extends React.Component {
  constructor(props) {
    super(props);
    
    this.dt = Object.keys(props.data)
    this.state = {
      country: this.dt[1],
      wd: document.getElementById("powerman5-app-root").offsetWidth,
    };

    bind(this, 'update', 'handleResize');
  }

  update(country){
    this.setState({country: country})
  }
  
  componentDidMount() {     
    window.addEventListener('resize', this.handleResize);
  }

  handleResize() {
    let newWd = document.getElementById("powerman5-app-root").offsetWidth;
    if(newWd != this.state.wd){
      this.setState({
        wd: newWd
      })
    }
  }

  render() {

    let mobileView = (this.state.wd <= 800)


    const label = "GLOBAL"
    const header = (mobileView) ? "Explore by country" : this.props.header;
    const data = Object.keys(this.props.data)
    const glob = data[0];
    const dt = data.splice(1);

    return (
      <div id="pm-graph5" className="pm-graph">
        
         { mobileView ? (
            <div>
              <div id="pm-controls5">          
                <div id="pm-label5-2" className="pm-label">{header}</div>
                <CustomSelect 
                  className="customSelect" 
                  data={dt} 
                  update={this.update}
                /> 
              </div>

            <BarChart3 
              data={this.props.data}
              country={this.state.country}
            />              


            </div>
          ) : (
            <div id="pm-graph5-container">
            <div id="pm-controls5">          
              <div id="pm-label5-1" className="pm-label">{label}</div>
              <div id="pm-label5-2" className="pm-label">{header}</div>
              <CustomSelect 
                wd={200}
                className="customSelect" 
                data={dt} 
                update={this.update}
              /> 
            </div>
            
            <BarChart2 
              data={this.props.data}
              country={this.state.country}
            />
            </div>
          )}

      </div>
    );
  }
}

