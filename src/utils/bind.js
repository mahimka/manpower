export default function bind(obj, ...args) {
  args.forEach(arg => {
    obj[arg] = obj[arg].bind(obj);
  });
}
